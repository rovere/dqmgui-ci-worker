# Start from the base SLC6 slave images
FROM gitlab-registry.cern.ch/ci-tools/ci-worker:cc7

ENV INSTALL_DIR=/data/srv
ENV HTTPG_VERSION=HG1904a
ENV ARCH=slc7_amd64_gcc630
ENV COMP=comp
EXPOSE 8060

# Clean YUM cache and install yum-plugin-ovl, so no package checksum errors pop-up
# https://github.com/CentOS/sig-cloud-instance-images/issues/15
RUN rpm --rebuilddb && yum -y install yum-plugin-ovl && yum clean all

# Packages required by cmssw bootstrap.sh
RUN yum -y install sudo zsh e2fsprogs e2fsprogs-libs libXcursor libXrandr libXi libXinerama mesa-libGLU mesa-libGL \
    openssl libXft-devel libX11-devel libXpm-devel libXext-devel mesa-libGLU-devel perl-Switch perl-Env \
    # Packages required by setup tutorial
    # https://twiki.cern.ch/twiki/bin/view/CMS/DQMGuiForUsers#How_to_set_up_your_own_DQM_GUI_f
    tk compat-readline5 perl-ExtUtils-Embed compat-libstdc++-33 libXmu libXpm \
    # Xvfb sed to start firefox in a virtual display
    firefox Xvfb \
    # Clean yum caches for reduced layer size
    && yum clean all

# Prepare user and work directory
RUN useradd -u 1001 dqmgui && \
    mkdir -p ${INSTALL_DIR} && \
    chown dqmgui:dqmgui ${INSTALL_DIR} && \
    chmod -R 777 /opt/app-root/jenkins

WORKDIR ${INSTALL_DIR}

# CMS DMWM Deploymnet. Deploy command needs to be executed on a non sudo user. Setting 777 for when Jenkins executes
RUN git clone git://github.com/dmwm/deployment.git && \
    sudo -u dqmgui ${INSTALL_DIR}/deployment/Deploy -r "comp=${COMP}" -A ${ARCH} -R comp@${HTTPG_VERSION} -t ${HTTPG_VERSION} -s "prep sw post" ${INSTALL_DIR} dqmgui/bare && \
    chmod -R 777 ${INSTALL_DIR}

# Privileged containers (root user) are not allowed in shared infrastructure according to
# https://jenkinsdocs.web.cern.ch/chapters/slaves/builtin.html
# USER root
