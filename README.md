# Customer worker image for running DQMGUI CI builds for Jenkins

This is a fork from https://gitlab.cern.ch/gitlabci-examples/custom_ci_worker

This project builds one docker image based on SLC6 that is used as a custom slave when building
[DQMGUI](ttps://github.com/rovere/dqmgui) application.
The Jenkins instance setup to use this docker image can be found
[here](https://dqmgui-jenkins.web.cern.ch/).

To read more about Jenkins and Docker slave in CERN read
[documentation](http://cern.ch/jenkinsdocs/chapters/slaves/custom-docker.html)
from CERN IT.

## Run DQMGUI from docker image your self

To run your own instance of DQMGUI using this docker image run this:
``` 
docker run -itd --net=host -p 8060:8060 -v ${PWD}/dqmgui/:/data/srv/BUILD gitlab-registry.cern.ch/asinica/dqmgui-ci-worker:slc6 bash
```

Note: `-v` option mounts a local directory `${PWD}/dqmgui/` to a directory in the docker image.
This is used to mount your version of dqmgui code, than can be patched using `monDistPatch -s DQM`

After you have launched the docker command above, your docker container is started. 
You still have to patch/start dqmgui manually inside this docker container.

To open an active shell within docker container ("login"), run this command:
```
docker exec -it -u 0 <CONTAINER_ID> bash
```

Replace `<CONTAINER_ID>` with output from previous `docker run` command,
or run `docker ps` to get a list currently running docker containers. 